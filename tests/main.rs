extern crate termion;
extern crate ttymer;

use std::io::{stdout, Write};
use std::thread::sleep;
use std::time::Duration;

use termion::color::*;
use termion::raw::IntoRawMode;
use termion::*;

use ttymer::terminal::clear;
use ttymer::time::Time;

#[test]
fn it_parses_durations() {
    assert_eq!(Time::from_duration(&Duration::new(3600, 0)), Time::new(1, 0, 0, 0));
    assert_eq!(Time::from_duration(&Duration::new(8651, 0)), Time::new(2, 24, 11, 0));
    assert_eq!(Time::from_duration(&Duration::new(0, 1234000)), Time::new(0, 0, 0, 1));
    assert_eq!(Time::from_duration(&Duration::new(0, 12_340_000)), Time::new(0, 0, 0, 12));
    assert_eq!(Time::from_duration(&Duration::new(0, 3_422_340_000)), Time::new(0, 0, 3, 422));
}

#[test]
#[should_panic]
fn it_rejects_really_messy() {
    Time::new(90, 90, 90, 90);
}

#[test]
#[should_panic]
fn it_rejects_bad_minutes() {
    Time::new(0, 181, 0, 0);
}

#[test]
#[should_panic]
fn it_rejects_bad_secs() {
    Time::new(0, 0, 80, 1);
}

#[test]
fn it_takes_special_minutes() {
    assert_eq!(Time::new(0, 90, 0, 0), Time::new(1, 30, 0, 0));
    assert_eq!(Time::new(0, 180, 0, 0), Time::new(3, 0, 0, 0));
}

#[test]
fn it_takes_special_secs() {
    assert_eq!(Time::new(0, 0, 90, 0), Time::new(0, 1, 30, 0));
}

#[test]
fn parses_strings() {}

#[test]
fn it_pretty_prints_time1() {
    println!("{}", Time::from_string("30"));
    println!("{}", Time::from_string("5:30"));
    println!("{}", Time::from_string("1:00:30"));
    println!("{}", Time::from_string("1:15:30:500"));
}

#[test]
fn it_tests_colours() {
    clear(String::new());
    for hue in 0..150 {
        draw_block(hsl_to_rgb(hue, 1.0, 0.4), format!("{}", hue));
        sleep(Duration::from_millis(50));
    }
    clear(String::new());

    #[allow(clippy::many_single_char_names)]
    fn hsl_to_rgb(h: u32, s: f32, l: f32) -> Bg<Rgb> {
        // https://stackoverflow.com/questions/2353211/hsl-to-rgb-color-conversion
        if s == 0.0 {
            color::Bg(color::Rgb(000, 000, 000))
        } else {
            fn hue_to_rgb(p: f32, q: f32, t: f32) -> f32 {
                let mut t = t;
                if t < 0.0 {
                    t += 1.0;
                }
                if t > 1.0 {
                    t -= 1.0;
                }
                if t < 1.0 / 6.0 {
                    return p + (q - p) * 6.0 * t;
                }
                if t < 1.0 / 2.0 {
                    return q;
                }
                if t < 2.0 / 3.0 {
                    return p + (q - p) * (2.0 / 3.0 - t) * 6.0;
                }
                p
            }
            fn floor_color(val: f32) -> u8 {
                (val * 256.0).floor().min(255.0) as u8
            }
            let q = if l < 0.5 { l * (1.0 + s) } else { l + s - l * s };
            let p = 2.0 * l - q;
            let h = h as f32 / 360.0;
            let r = floor_color(hue_to_rgb(p, q, h + 1.0 / 3.0));
            let g = floor_color(hue_to_rgb(p, q, h));
            let b = floor_color(hue_to_rgb(p, q, h - 1.0 / 3.0));
            color::Bg(color::Rgb(r, g, b))
        }
    }

    fn draw_block<S: Into<String>>(color: color::Bg<color::Rgb>, message: S) {
        let mut stdout = stdout().into_raw_mode().unwrap();
        let mut out: String = String::new();
        for _ in 0..10 {
            out.push_str("          \n\r");
        }
        out.push_str(message.into().as_str());
        write!(
            stdout,
            "{}{}{}{}\n\r{}{}",
            termion::cursor::Goto(1, 1),
            termion::cursor::Hide,
            color,
            out,
            color::Fg(color::Reset),
            termion::cursor::Show,
        )
        .unwrap();
        stdout.flush().unwrap();
    }
}
