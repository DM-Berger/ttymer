#![allow(unknown_lints)]

// extern crate clap;
extern crate libc;
extern crate nix;
extern crate notify_rust;
extern crate termion;

use std::string::String;
use std::time::Instant;

// external
// #[allow(unused_imports)]
// use termion::input::TermRead; // needed for stdin.keys()

// project
mod chars;
mod os;
mod state;
mod terminal;
mod time;
mod cli;

use time::Time;

fn main() {
    // should get time even before parsing args, to ensure accuracy
    let start = Instant::now();
    let config = cli::get_config();
    println!("{:?}", config);

    state::set_width(termion::terminal_size().unwrap().0);
    let args: String = std::env::args().nth(1).unwrap_or_else(|| String::from("20"));
    let end = Time::from_string(&args);

    #[cfg(unix)]
    os::unix::setup_signal_handlers();

    #[cfg(target_os = "macos")]
    let volume: Option<u32> = {
        if config.notify { Some(os::apple::get_sound()) } else {None}
    };


    terminal::run_countdown(&start, &end); // checks STOP
    terminal::reset(); // cleanup, ignores STOP
    if config.notify {
        os::notify_with_volume(100); // checks STOP
    }

    #[cfg(target_os = "macos")]
    if config.notify {
        os::apple::set_sound(&volume.unwrap()); // cleanup, ignores STOP
    }

    println!("\nTimer shut down.");
}
