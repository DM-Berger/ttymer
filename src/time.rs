use std::time::Duration;

use chars::{BlockChar, DigitSize};

#[derive(Debug, PartialEq)]
pub struct Time {
    hours: u32,
    mins: u32,
    secs: u32,
    millis: u32,
}

impl std::fmt::Display for Time {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        if self.hours == 0 && self.mins == 0 && self.secs == 0 {
            write!(f, "00h : 00min : 00s : {:03}ms", self.millis)
        } else if self.hours == 0 && self.mins == 0 {
            write!(f, "00h : 00min : {:02}s : {:03}ms", self.secs, self.millis)
        } else if self.hours == 0 {
            write!(f, "00h : {:02}min : {:02}s : {:03}ms", self.mins, self.secs, self.millis)
        } else {
            write!(f, "{:01}h : {:02}min : {:02}s : {:03}ms", self.hours, self.mins, self.secs, self.millis)
        }
    }
}

impl Time {
    pub fn new(hours: u32, mins: u32, secs: u32, millis: u32) -> Self {
        if millis > 999 {
            panic!("Only milliseconds less than 1000 are valid");
        };
        // if secs > 90 {
        //     panic!("Only seconds up to 90 are valid");
        // };
        // if mins > 180 {
        //     panic!("Only minutes up to 180 are valid");
        // };
        if secs > 59 && hours == 0 && mins == 0 && millis == 0 {
            Self { hours, mins: secs / 60, secs: secs % 60, millis }
        } else if secs > 59 && (hours != 0 || mins != 0 || millis != 0) {
            panic!("Seconds in [60,90] are only valid when other values are all zero.");
        } else if mins > 59 && hours == 0 && secs == 0 && millis == 0 {
            Self { hours: mins / 60, mins: mins % 60, secs, millis }
        } else if mins > 59 && (hours != 0 || secs != 0 || millis != 0) {
            panic!("Minutes in [60,180] are only valid when other values are all zero.");
        } else {
            Self { hours, mins, secs, millis }
        }
    }
    pub fn from_string(arg: &str) -> Self {
        if arg.contains('.') {
            let all_times = arg.split(&['.', ':'][..]).collect::<Vec<&str>>();
            let times = all_times[0..all_times.len()]
                .map(|time: &str| {
                    time.parse::<u32>()
                        .expect("Something other than positive integers passed as args.")
                })
                .collect::<Vec<u32>>();
            }
            let millis = all_times[all_times.len() - 1].parse::<u32>;
        let times = arg
            .split(':')
            .map(|time: &str| {
                time.parse::<u32>()
                    .expect("Something other than positive integers passed as args.")
            })
            .collect::<Vec<u32>>();
        let millis = .split(':').collect()
        match times.len() {
            1 => Time::new(0, 0, times[0], 0),
            2 => Time::new(0, times[0], times[1], 0),
            3 => Time::new(times[0], times[1], times[2], 0),
            4 => Time::new(times[0], times[1], times[2], times[3]),
            _ => panic!("Bad input"),
        }
    }
    pub fn from_duration(duration: &Duration) -> Self {
        let millis = duration.subsec_millis();
        let secs = duration.as_secs() as u32;
        let mins = (secs / 60) % 60;
        let hours = secs / 3600;
        let secs = secs % 60;
        Self { hours, mins, secs, millis }
    }
    pub fn as_string(&self) -> String {
        format!("{}", self)
    }
    pub fn as_blockchar_string(&self, size: &DigitSize) -> String {
        use self::DigitSize::*;

        let mut out = String::new();
        let string_rep = self.as_string();
        let mut block_chars: Vec<Vec<&'static str>> = Vec::with_capacity(string_rep.len());
        for ref ch in string_rep.chars() {
            match *ch {
                '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' => {
                    let digit = ch.to_digit(10).unwrap() as usize;
                    match *size {
                        Large => block_chars.push(BlockChar::Digit::Large(digit)),
                        Medium => block_chars.push(BlockChar::Digit::Medium(digit)),
                        Small => block_chars.push(BlockChar::Digit::Small(digit)),
                        Tiny => {}
                    };
                }
                ':' => match *size {
                    Large => block_chars.push(BlockChar::Colon::Large()),
                    Medium => block_chars.push(BlockChar::Colon::Medium()),
                    Small => block_chars.push(BlockChar::Colon::Small()),
                    Tiny => {}
                },
                _ => {}
            }
        }
        if !block_chars.is_empty() {
            let char_height = block_chars[0].len();
            for row in 0..char_height {
                for block_char in &block_chars {
                    out.push_str(&*block_char[row]);
                    out.push(' ');
                }
                out.push_str("\n\r");
            }
            out
        } else {
            string_rep
        }
    }
    pub fn as_duration(&self) -> Duration {
        let millis = u64::from(self.hours * 3_600_000 + self.mins * 60_000 + self.secs * 1000 + self.millis);
        Duration::from_millis(millis)
    }
}
