extern crate clap;
extern crate libc;
extern crate nix;
extern crate notify_rust;
extern crate termion;

pub mod chars;
pub mod cli;
pub mod os;
pub mod state;
pub mod terminal;
pub mod time;
