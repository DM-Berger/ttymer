use std::io::{stdin, stdout, Write};
use std::thread;
use std::time::Duration;
use std::time::Instant;
use termion::color;
use termion::color::{Fg, Rgb};
use termion::event::Key;
use termion::input::TermRead;
use termion::raw::IntoRawMode;

use chars::DigitSize;
use state;
use time::Time;

// min and max hues, in HSL model
const HUE_START: u8 = 150;
const HUE_END: u8 = 0;

pub fn reset() {
    // note this function should NOT check STOP since we want to use it
    // to do cleanup no matter what
    let mut stdout = stdout().into_raw_mode().unwrap();
    write!(stdout, "{}{}", termion::style::Reset, termion::cursor::Show,).unwrap();
}

pub fn clear<S: Into<Option<String>>>(text: S) {
    let mut stdout = stdout().into_raw_mode().expect("Failed to acquire stdout.");
    match text.into() {
        Some(s) => {
            write!(
                stdout,
                "{}{}{}{}",
                s,
                termion::cursor::Goto(1, 1),
                termion::clear::All,
                termion::cursor::Goto(1, 1),
            )
            .expect("Error writing to stdout.");
        }
        None => {
            write!(stdout, "{}{}{}", termion::cursor::Goto(1, 1), termion::clear::All, termion::cursor::Goto(1, 1),)
                .expect("Error writing to stdout.");
        }
    }
}

pub fn draw_countdown_frame(now: &Time, hue: u8, size: &DigitSize) {
    let mut stdout = stdout().into_raw_mode().expect("Failed to acquire stdout.");
    write!(
        stdout,
        "{}{}{}{}\n\r",
        termion::cursor::Goto(1, 1),
        termion::cursor::Hide,
        hsl_to_rgb(hue, 1.0, 0.35),
        now.as_blockchar_string(size),
    )
    .expect("Error writing to stdout.");
    stdout.flush().expect("Error flushing buffer to stdout.");
}

pub fn start_controls() {
    thread::spawn(|| {
        let stdin = stdin();
        // this "for" is actually an infinite iterator over key events
        for key in stdin.keys() {
            match key.unwrap() {
                Key::Char('\n') | Key::Char('\r') | Key::Char(' ') | Key::Char('p') | Key::Char('P') => {
                    state::pause();
                    print!("{}Paused.", termion::clear::CurrentLine,);
                }
                Key::Esc | Key::Char('q') | Key::Ctrl('c') => {
                    state::kill();
                    print!("{}Quitting...", termion::clear::CurrentLine,);
                    break;
                }
                Key::Ctrl('z') => {
                    state::stop();
                    print!("{}Stopped.", termion::clear::CurrentLine,);
                    break;
                }
                //                    Key::Char('r') => {
                //                        println!("Resuming...");
                //                        state::unpause();
                //                    }
                _ => {
                    print!("{}", termion::clear::CurrentLine);
                }
            }
        }
    });
}

pub fn run_countdown(start: &Instant, end: &Time) {
    let end_dur = end.as_duration();
    let total_dur = end_dur - start.elapsed();

    let hue_range = u32::from(std::cmp::max(HUE_START, HUE_END) - std::cmp::min(HUE_START, HUE_END));
    let hue_is_decreasing = HUE_START > HUE_END;
    let hue_interval = total_dur.checked_div(hue_range).expect("Hue range is empty.");

    let mut intervals_passed: u8 = 0;
    let mut current_hue = HUE_START;
    clear(None);
    start_controls();
    let mut size = state::width();
    loop {
        if !state::going() || start.elapsed() >= end_dur {
            break;
        }
        let now = start.elapsed();
        let nowtime = end_dur.checked_sub(now).unwrap();

        // resize output only if neccessary
        let current_size: usize = state::width();
        if current_size != size {
            size = current_size;
            clear(None);
        }
        draw_countdown_frame(&Time::from_duration(&nowtime), current_hue, &DigitSize::from_term_width(current_size));

        if now > hue_interval.checked_mul(u32::from(intervals_passed + 1)).unwrap() {
            if hue_is_decreasing {
                current_hue = current_hue.saturating_sub(1);
            } else {
                current_hue = current_hue.saturating_add(1);
            }
            intervals_passed += 1;
        }
        // sleep so that we get about 60fps performance
        thread::sleep(Duration::from_millis(10));
    }
    draw_countdown_frame(end, HUE_END, &DigitSize::from_term_width(state::width()));
}

/// Takes HSL color, where h is in [0,360], s, and l are in [0, 1]
#[allow(clippy::many_single_char_names)]
fn hsl_to_rgb(h: u8, s: f32, l: f32) -> Fg<Rgb> {
    // https://stackoverflow.com/questions/2353211/hsl-to-rgb-color-conversion
    if s == 0.0 {
        color::Fg(color::Rgb(000, 000, 000))
    } else {
        fn hue_to_rgb(p: f32, q: f32, t: f32) -> f32 {
            let mut t = t;
            if t < 0.0 {
                t += 1.0;
            }
            if t > 1.0 {
                t -= 1.0;
            }
            if t < 1.0 / 6.0 {
                return p + (q - p) * 6.0 * t;
            }
            if t < 1.0 / 2.0 {
                return q;
            }
            if t < 2.0 / 3.0 {
                return p + (q - p) * (2.0 / 3.0 - t) * 6.0;
            }
            p
        }
        fn floor_color(val: f32) -> u8 {
            (val * 256.0).floor().min(255.0) as u8
        }
        let q = if l < 0.5 { l * (1.0 + s) } else { l + s - l * s };
        let p = 2.0 * l - q;
        let h = f32::from(h) / 360.0;
        let r = floor_color(hue_to_rgb(p, q, h + 1.0 / 3.0));
        let g = floor_color(hue_to_rgb(p, q, h));
        let b = floor_color(hue_to_rgb(p, q, h - 1.0 / 3.0));
        color::Fg(color::Rgb(r, g, b))
    }
}
