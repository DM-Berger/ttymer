extern crate clap;

use self::clap::{App, Arg, ArgMatches};
use time::Time;

#[derive(Debug)]
pub struct Config {
    pub time: Time,
    pub notify: bool,
}

impl Config {
    pub fn new(time: Time, notify: bool) -> Self {
        Self {time, notify}
    }
}

fn get_app<'a, 'b>() -> App<'a, 'b> {
    App::new("ttymer")
        .version("0.1")
        .author("Derek Berger <dmberger.dev@gmail.com>")
        .about("A minimialist CLI-based countdown timer")
        .arg(
            Arg::with_name("time")
                .value_name("TIMESTAMP")
                .help("How long to run the timer.")
                .long_help(
"Specify a timestamp to countdown from. Full timestamp format is HH:MM:SS.mm
for hours (H), minutes (M), seconds (S), and milliseconds (m) but various short
form and alternate formats are accepted:

    S        e.g. `ttymer 2` for a 2-second timer
    SS       e.g. `ttymer 90` for a 90-second timer
    MM:SS    e.g. `ttymer 20:00` for a 20-minute timer
    S.mm     e.g. `ttymer 2.5` for a 2-second and 500 ms timer

For convenience, you can specify hours and minutes greater than 60. These will
be converted to display normally.")
                .takes_value(true)
                .default_value("20")
        )
        .arg(
            Arg::with_name("notify")
                .long("notify")
                .short("n")
                .value_name("NOTIFY")
                .help("Whether to create an OS-specific notifiction on timer end.")
                .takes_value(false)
        )
}

pub fn get_config() -> Config {
    let matches = get_app().get_matches();
    let t = matches.value_of("time").unwrap();
    let time = Time::from_string(t);
    let notify = matches.is_present("notify");
    Config::new(time, notify)
}

#[test]
pub fn test_get_time() {
    let matches = get_app().get_matches_from(vec!["ttymer", "20:30"]);
    if let Some(t) = matches.value_of("time") {
        assert!(t == "20:30")
    }
}


#[test]
pub fn test_get_notify() {
    let matches = get_app().get_matches_from(vec!["ttymer", "--notify"]);
    assert!(matches.is_present("notify"))
}