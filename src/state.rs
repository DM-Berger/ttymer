use std::sync::atomic::AtomicBool;
use std::sync::atomic::AtomicUsize;
use std::sync::atomic::Ordering::Relaxed;

static TERMWIDTH: AtomicUsize = AtomicUsize::new(0);
static KILLED: AtomicBool = AtomicBool::new(false);
static STOPPED: AtomicBool = AtomicBool::new(false);
static PAUSED: AtomicBool = AtomicBool::new(false);

pub fn kill() {
    KILLED.store(true, Relaxed);
}

pub fn stop() {
    STOPPED.store(true, Relaxed);
}
pub fn pause() {
    PAUSED.store(true, Relaxed);
}
pub fn unpause() {
    PAUSED.store(false, Relaxed);
}

pub fn killed() -> bool {
    KILLED.load(Relaxed)
}
pub fn stopped() -> bool {
    STOPPED.load(Relaxed)
}
pub fn paused() -> bool {
    PAUSED.load(Relaxed)
}
pub fn going() -> bool {
    !killed() && !stopped() && !paused()
}

pub fn set_width(width: u16) {
    TERMWIDTH.store(width as usize, Relaxed);
}
pub fn width() -> usize {
    TERMWIDTH.load(Relaxed)
}
