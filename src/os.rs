#[cfg(all(unix, not(target_os = "macos")))]
extern crate notify_rust;

use std::process::Command;
use std::thread;
use std::time::{Instant,Duration};
use nix::sys::signal;
use nix::sys::signal::{sigaction, SigAction, SigHandler, SigSet};
#[cfg(all(unix, not(target_os = "macos")))]
use notify_rust::NotificationUrgency::*;
#[cfg(all(unix, not(target_os = "macos")))]
use notify_rust::{Notification, NotificationHint, Timeout};
use state;

// relies on macos specific functionality (e.g. AppleScript)
#[cfg(target_os = "macos")]
pub mod apple {
    use super::*;

    const APPLENOTIFY: &str = "display notification \"Timer!\" with title \"Timer Done!\" \
                                   sound name \"Glass\"";

    const APPLEALERT: &str = "display alert \"Timer!\" message \"The timer is done!\" as \
                                  warning";

    const GETSOUND: &str = "error \"NOT A REAL ERROR: this error is a hack to return the \
                                sound value [+1000] to the caller.\" number (get output volume of \
                                (get volume settings)) + 1000";

    pub fn run_apple_script<S, E>(script: S, message: E) -> std::process::Output
    where
        S: Into<String>,
        E: Into<&'static str>,
    {
        // this function must be allowed to run regardless of state of
        // STOP, since it will occasionally perform cleanup
        Command::new("osascript")
            .arg("-e")
            .arg(script.into())
            .output()
            .expect(message.into())
    }

    pub fn dialog_alert() {
        if state::going() {
            run_apple_script(APPLEALERT, "Failed to execute AppleScript alert");
        }
    }

    pub fn notify() {
        for _ in 0..5 {
            let start = Instant::now();
            if state::going() {
                run_apple_script(APPLENOTIFY, "Failed to execute AppleScript notification");
            }
            while state::going() && start.elapsed() < Duration::new(2, 0) {
                thread::sleep(Duration::from_millis(10));
            }
        }
        dialog_alert();
    }

    pub fn get_sound() -> u32 {
        let out = run_apple_script(GETSOUND, "Failed to get sound value.");
        let err_out = String::from_utf8(out.stderr).expect("Failed to unwrap AppleScript error exit code.");
        let volume_start = err_out.find('(').unwrap() + 1;
        let volume: u32 = (&err_out[volume_start..volume_start + 4])
            .parse()
            .expect("Error parsing stderr.");
        volume - 1000
    }

    pub fn set_sound(volume: &u32) {
        run_apple_script(format!("set volume output volume {}", volume), "Failed to set sound.");
    }
}

#[cfg(unix)]
pub mod unix {
    use super::*;

    extern "C" fn sigwinch_handler(_: nix::libc::c_int) {
        state::set_width(termion::terminal_size().unwrap().0);
    }

    extern "C" fn sigint_sigterm_handler(_: nix::libc::c_int) {
        state::kill();
    }
    extern "C" fn sigtstp_handler(_: nix::libc::c_int) {
        state::stop();
    }

    pub fn setup_signal_handlers() {
        unsafe {
            sigaction(
                nix::sys::signal::Signal::SIGWINCH,
                &SigAction::new(SigHandler::Handler(sigwinch_handler), signal::SA_RESTART, SigSet::empty()),
            )
            .expect("failed to set SIGWINCH handler");

            sigaction(
                nix::sys::signal::Signal::SIGTERM,
                &SigAction::new(SigHandler::Handler(sigint_sigterm_handler), signal::SA_RESTART, SigSet::empty()),
            )
            .expect("failed to set SIGTERM handler");

            sigaction(
                nix::sys::signal::Signal::SIGINT,
                &SigAction::new(SigHandler::Handler(sigint_sigterm_handler), signal::SA_RESTART, SigSet::empty()),
            )
            .expect("failed to set SIGINT handler");

            sigaction(
                nix::sys::signal::Signal::SIGTSTP,
                &SigAction::new(SigHandler::Handler(sigtstp_handler), signal::SA_RESTART, SigSet::empty()),
            )
            .expect("failed to set SIGTSTP handler");
        }
    }

    #[cfg(all(unix, not(target_os = "macos")))]
    pub mod nomac {
        use super::*;

        const SOUND: &str = "message-new-instant";

        pub fn alert() {
            Notification::new()
                .summary("Timer!")
                .body("Terminal timer is done!")
                .sound_name(SOUND)
                .hint(NotificationHint::Resident(true)) // won't work on KDE
                .timeout(Timeout::Never) // make persist on KDE and gnome
                .show()
                .expect("Failed to show notification.");
        }
    }
}

#[allow(unused_variables)]
pub fn notify_with_volume(volume: u32) {
    if state::going() {
        #[cfg(target_os = "macos")]
        {
            let start_volume = apple::get_sound();
            apple::set_sound(&volume);
            apple::notify();
            apple::set_sound(&start_volume);
        }

        #[cfg(all(unix, not(target_os = "macos")))]
        unix::nomac::alert()
    }
}
