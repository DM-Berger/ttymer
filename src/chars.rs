#[allow(non_snake_case)]
pub mod BlockChar {
    pub mod Digit {
        #[rustfmt::skip]
        pub fn Large(digit: usize) -> Vec<&'static str> {
            vec![
                vec![" ▄▄▄▄ ", "█    █", "█    █",  "█    █", "▀▄▄▄▄▀"],  // 0
                vec!["   ▄  ",  "  ▀█  ", "   █  ",  "   █  ", "  ▄█▄ " ],    // 1
                vec![" ▄▄▄▄ ", "▀    █", "   ▄▀ ",  " ▄▀   ", "█▄▄▄▄▄"],  // 2
                vec![" ▄▄▄▄ ", "▀    █", " ▄▄▄▄▀", "     █", "▀▄▄▄▄▀"], // 3
                vec!["    ▄ ",  "  ▄▀█ ", "▄▀  █ ", "▀▀▀▀█▀", "    █ "],  // 4
                vec!["▄▄▄▄▄▄", "█     ", "▀▀▀▀▀▄", "     █", "▀▄▄▄▄▀"], // 5
                vec![" ▄▄▄▄ ", "█    ▀", "█▄▄▄▄ ", "█    █", "▀▄▄▄▄▀"], // 6
                vec!["▄▄▄▄▄▄", "    ▄▀", "   █  ",  "  █   ", "  █   "],   // 7
                vec![" ▄▄▄▄ ", "█    █", "▀▄▄▄▄▀", "█    █", "▀▄▄▄▄▀"], // 8
                vec![" ▄▄▄▄ ", "█    █", "▀▄▄▄▄█", "     █", "▀▄▄▄▄▀"], // 9
            ][digit].clone()
        }

        pub fn Medium(digit: usize) -> Vec<&'static str> {
            vec![
                vec![" ▄▄▄ ", "█   █", "█   █", "▀▄▄▄▀"],       // 0
                vec!["  ▄  ", " ▀█  ", "  █  ", " ▄█▄ "],                 // 1
                vec![" ▄▄▄ ", "▀   █", "  ▄▀ ", "▄█▄▄▄"],       // 2
                vec![" ▄▄▄ ", "▀   █", "  ▀▀▄", "▀▄▄▄▀"],     // 3
                vec!["▄  ▄ ", "█  █ ", "▀▀▀█▀", "   █ "],           // 4
                vec!["▄▄▄▄▄", "█▄▄▄ ", "    █", "▀▄▄▄▀"], // 5
                vec![" ▄▄▄ ", "█   ▀", "█▀▀▀▄", "▀▄▄▄▀"], // 6
                vec!["▄▄▄▄▄", "   ▄▀", "  █  ", " █   "],             // 7
                vec![" ▄▄▄ ", "█   █", "▄▀▀▀▄", "▀▄▄▄▀"], // 8
                vec![" ▄▄▄ ", "█   █", " ▀▀▀█", "▀▄▄▄▀"],   // 9
            ][digit]
                .clone()
        }

        pub fn Small(digit: usize) -> Vec<&'static str> {
            vec![
                vec![" ▄▄ ", "█  █", "▀▄▄▀"],       // 0
                vec![" ▄  ", "▀█  ", " █  "],               // 1
                vec!["▄▄▄ ", " ▄▄▀", "█▄▄▄"],   // 2
                vec!["▄▄▄ ", " ▄▄▀", "▄▄▄▀"],   // 3
                vec!["▄ ▄ ", "█▄█▄", "  █ "],         // 4
                vec!["▄▄▄▄", "█▄▄ ", "▄▄▄▀"], // 5
                vec![" ▄▄▄", "█▄▄ ", "▀▄▄▀"],   // 6
                vec!["▄▄▄▄", "  ▄▀", " █  "],         // 7
                vec![" ▄▄ ", "▀▄▄▀", "▀▄▄▀"],   // 8
                vec![" ▄▄ ", "▀▄▄█", " ▄▄▀"],     // 9
            ][digit]
                .clone()
        }
    }

    pub mod Colon {
        pub fn Large() -> Vec<&'static str> {
            vec!["    ", " █  ", "    ", " █  ", "    "]
        }
        pub fn Medium() -> Vec<&'static str> {
            vec!["   ", " ▀ ", " ▄ ", "   "]
        }
        pub fn Small() -> Vec<&'static str> {
            vec!["   ", " ▀ ", " ▀ "]
        }
    }
}

pub enum DigitSize {
    Large,
    Medium,
    Small,
    Tiny,
}

// instead implement a trait "FromTermWidth" for both DigitSize and ColonSize

impl DigitSize {
    pub fn from_term_width(width: usize) -> DigitSize {
        if width < 58 {
            DigitSize::Tiny
        } else if (58..66).contains(&width) {
            DigitSize::Small
        } else if (66..78).contains(&width) {
            DigitSize::Medium
        } else {
            DigitSize::Large
        }
    }
}

// see https://doc.rust-lang.org/book/second-edition/ch19-03-advanced-traits.html
pub trait DisplayChar {
    type Size; // will be Vec<Vec<&'static str>
}
