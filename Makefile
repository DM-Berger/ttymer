all: rust

rust:
	@cargo build

lint:
	@cargo +nightly clippy

run:
	@cargo run

macos:
	@osacompile -o AppleTimerAlert.scpt appleVolume.APPLESCRIPT
	@cargo +nightly clippy

.PHONY: all rust lint macos
